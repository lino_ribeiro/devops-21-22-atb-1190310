# DevOps - ca4-part1
---

## Containerization with Docker
---
---
## 1. Analysis, Design and Implementation

Containerization is a system of intermodal freight transport using intermodal containers, also called shipping containers and
ISO containers.

Containerization allows developers to create and deploy applications faster and more securely.

With traditional methods, code is developed in a specific computing environment which, when transferred to a new location,
often results in bugs and errors.

Containerization eliminates this problem by bundling the application code together with the related configuration files,
libraries, and dependencies required for it to run.

This single package of software or “container” is abstracted away from the host operating system, and hence,
it stands alone and becomes portable—able to run across any platform or cloud, free of issues.

Below you can see a brief comparison between Virtual Machines and Containers.

![image](../containers-vs-virtual-machines.jpg)

The image bellow shows you the workflow of the process behind the container creation:

![image](../Docker_Workflow.png)



The goal of this class assignment is to build containers to run a chat server, so that we can connect chat clients to it from our host machine. We will develop two versions:

In the first approach, we will build the chat server when we create the image (version1).

In the second approach, we will build the chat server in our host computer and copy the built .jar file into the image (version2).

Both images will be based on an exising image, ubuntu.

## Dockerfile commands

*FROM* : Indicates the base image;
*RUN* : Run a command during the build phase of the image;
*WORKDIR* : Change current working directory on the container;
*EXPOSE* : Exposes a port on the host;
*ENTRYPOINT* : Default application that is to be executed by the container;


## First Version

````
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-8-jdk-headless
RUN apt-get install -y git


RUN git clone https://lino_ribeiro@bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310.git

WORKDIR devops-21-22-atb-1190310/ca2/ca2-part1/gradle_basic_demo

RUN chmod u+x gradlew
RUN ./gradlew clean build

EXPOSE 59001

ENTRYPOINT ./gradlew runServer
````

## Second Version

````
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-11-jdk-headless

COPY basic_demo-0.1.0.jar basic_demo-0.1.0.jar

EXPOSE 59001

ENTRYPOINT java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
````

After create DockerFile, it is necessary to build the image of each Dockerfile:

````docker build -t my-image1````

````docker build -t my-image2````

Then, it is necessary create a tag about each image:

````docker tag my-image1:latest linoribeiro/my-image:latest````

````docker tag my-image2:latest linoribeiro/my-image2:latest````

Finally, we should push all the images:

````docker push linoribeiro/my-image:latest````

````docker push linoribeiro/my-image2:latest````

In my repository, it is possible see both images: 

![image](../RemoteRepository.JPG)