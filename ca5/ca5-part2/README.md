# Ca5-part2

## CI/CD Pipelines with Jenkins

This is the README for the part two of the fifth class assignment.

This class assignment subject is **CI/CD Pipelines with Jenkins**.

## 1. Analysis, Design and Implementation

### Continuous Integration/Continuous Deployment

### Issues

Optionally, you can create issues on Bitbucket to track the development of this class assignment. They could represent software features, story leads, user stories, etc.

For the development of this assigment, 5 issues can be created:

1. Create a new pipeline in jenkins;
2. Configure the jenkinsfile script;
3. Create a dockerfile;
4. Run the pipeline;


### Create a new pipeline (Issue 1)

Following all steps in previous README.md (ca5-part1), you should create a new pipeline.


### Configure the Jenkinsfile script (Issue 2)

```shell

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://lino_ribeiro@bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310'
            }
        }

        stage('Assemble') {
            steps {
               dir('ca2/ca2-part2/react-and-spring-data-rest-basic/') {
                    echo 'Assembling...'
                    bat 'gradlew assemble'
                }
            }
        }

		stage('Archiving') {
			steps {
				dir('ca2/ca2-part2/react-and-spring-data-rest-basic/') {
					echo 'Archiving...'
					archiveArtifacts 'build/libs/*'
				}
			}
		}

		stage('Test') {
            steps {
				dir('ca2/ca2-part2/react-and-spring-data-rest-basic/') {
                    echo 'Testing...'
                    bat 'gradlew test'
                }
            }
        }

		stage('Javadoc') {
			steps {
				dir('ca2/ca2-part2/react-and-spring-data-rest-basic/') {
					echo 'Generating Javadoc documentation...'
                        bat './gradlew javadoc'
                        publishHTML (target : [allowMissing: false,
                        alwaysLinkToLastBuild: true,
                        keepAll: true,
                        reportDir: './',
                        reportFiles: 'myreport.html',
                        reportName: 'My Reports',
                        reportTitles: 'The Report'])

				}
			}
		}

		stage('Publish image') {
			steps {
				dir('ca2/ca2-part2/') {
					script {
						script {
                         def test = docker.build("linoribeiro/devops_2022_1190310:${env.BUILD_ID}", "--no-cache .")
                         docker.withRegistry('https://registry.hub.docker.com ', 'DockerHubCredentials') {
                         test.push()
						}

					}
				}
			}
		}
	}
}

```

This jenkinsfile should be placed where you declared your pipeline jenkinsfile path.

This will create 6 stages:
1. Checkout - Will checkout to your git repository;
2. Assemble - Will assemble a clean build of the project;
3. Archiving - Will archive the generated artifact;
4. Test - Will execute clean tests and save its report;
5. Javadoc -  Will generate the javadoc of the project and publish it in Jenkins;
6. Publish image - Will generate a docker image with Tomcat and the war file and publish it in the Docker Hub.


### Create a Dockerfile (Issue 3)

```shell

FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

ADD devops-21-22-atb-1190310/ca2/ca2-part1/react-and-spring-data-rest-basic/build/libs/gradle-tut-basic-0.0.1-SNAPSHOT.jar /usr/local/tomcat/webapps/

EXPOSE 8080

```

This dockerfile will build a docker image for this project and expose the port 8080.

### Run the pipeline (Issue 4)

Run the pipeline

### Mark your repository with the tag ca5-part2 (Issue 6)

After finishing the assignment and completing the README file, the repository should be market with a new tag "ca5-part2".

We can do this using the following **Git** commands:

```shell
git tag -a ca5-part2 -m "End of ca5-part2 assignment"

git push origin ca5-part2
```



# Jenkins vs Buddy

When comparing Buddy vs Jenkins, the Slant community recommends Jenkins for most people.

In the question “What are the best continuous integration tools?” Jenkins is ranked 4th while Buddy is ranked 14th.
The most important reason people chose Jenkins is: Jenkins is a free and open source continuous integration tool.


### What are the advantages of Jenkins?

- It is open source and it is user-friendly, easy to install and does not require additional installations or components

- It is free of cost

- Easily Configurable

  Jenkins can be easily modified and extended. It deploys code instantly, generates test reports. Jenkins can be configured according to the requirements for continuous integrations and continuous delivery.

- Platform Independent

  Jenkins is available for all platforms and different operating systems, whether OS X, Windows or Linux.

- Rich Plugin ecosystem

  The extensive pool of plugins makes Jenkins flexible and allows building, deploying and automating across various platforms.

- Easy support

  Because it is open source and widely used, there is no shortage of support from large online communities of agile teams.

- Most of the integration work is automated.

- Docker in conjunction with Jenkins is having a profound effect on development teams

  Everyone knows that Docker streamlines development and makes deployment vastly easier. Together Docker, Jenkins and its integrated ecosystem provide the coordinating software infrastructure for agile development.

### What are the disadvantages of Jenkins?

- Outdated interface

- Not user-friendly compared to current UI trends

- Its configuration is tricky

- Redundant and less updated plugins

- Not all of its plugins are compatible with declarative pipeline

- Lots of out of date documentation


### What are the advantages of Buddy?

- 15-minute configuration via GUI with instant export to YAML

- Isolated build containers ensure compatibility across the whole team

- Full Docker and Kubernetes support

- Available in cloud and on-premises

- Lightning-fast deployments based on changesets

- Caches dependencies and Docker layers for faster builds

- Integrates with AWS, Google Cloud, Azure, DigitalOcean...

- Supports all popular languages & frameworks

- Appealing documentation & responsive engineer support

### What are the disadvantages of Buddy?

- It can be expensive if you need a lot of parallel jobs running

- There’s a 90-minute timeout for build actions

- Buddy does not support C#
