# DevOps - ca4-part2
---

## Containerization with Docker

In ca4-part2 README file, it was an introduction about Docker so that, in this README file,  I only will describe the implementation.

### 1. Using Docker to set up a containerized environment to execute the Tutorial Spring Boot Application

In this part of the class assignment we will create a docker compose that setups two containers and execute the tut basic project developed in CA3 - Part 2.

#### Initial Setup

- Start by creating a new folder with the name ca4/docker.

- Clone this [repository](https://bitbucket.org/atb/docker-compose-spring-tut-demo/) so that you can get the docker-compose.yml file and the dockerfiles in web and db folder. This files will allow you to have two containers (one for the frontend and one for the database):

    - docker_web is the web container;

    - docker_db is the db container;

- Copy the following folders and file from the repository you've just clone to the folder ca4/docker:

    - data
    - db
    - web
    - docker-compose.yml

#### Changes in Dockerfile in web folder

Make some changes in the Dockerfile in the web folder:
```
FROM tomcat:9.0-jdk11

RUN apt-get update -y

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://lino_ribeiro@bitbucket.org/atb/docker-compose-spring-tut-demo.git

WORKDIR /tmp/build/tut-basic-gradle-docker

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/


EXPOSE 8080
```

#### Build the images based on the dockerfiles and run multi-container applications

Now that we are all setup, open the terminal in your local machine and change the path to the folder where the docker-compose.yml is located:

```
> cd C:\git\repos\devops-21-22-atb-1190310\ca4\ca4-part2
```

Build the images based on the dockerfiles and run the multi-container by using the command:

```
> docker-compose up
```

#### Check if the containers are running

To check if the multi-container is running, type:

```
> docker info
```

To check if the images that are used by the containers were built, type:

```
> docker images
```

Note: You can also open Docker dashboard to see if the containers are running!


### Publish the images on Docker Hub


First you need to create an account on Docker Hub.

After that, open the terminal in your local machine and change the path to the folder where the docker-compose.yml is located:

```
C:\git\repos\devops-21-22-atb-1190310\ca4\ca4-part2
```

To check your images, run the command:

```
> docker images
```

Then, it is necessary create a tag about each image:

````docker tag ca4-part2_web:latest linoribeiro/ca4-part2_web:latest````

````docker tag ca4-part2_db:latest linoribeiro/ca4-part2_db:latest````

Finally, we should push all the images:

````docker push linoribeiro/ca4-part2_db:latest````

````docker push linoribeiro/ca4-part2_web:latest````

In my repository, it is possible see both images: 

![Image](DockerHub.JPG)


## 2. Kubernetes as a complement to Docker

Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation.

Why you need Kubernetes and what it can do?

Containers are a good way to bundle and run your applications. In a production environment, you need to manage the containers that run the applications and ensure that there is no downtime. For example, if a container goes down, another container needs to start. Wouldn't it be easier if this behavior was handled by a system?

That's how Kubernetes comes to the rescue! Kubernetes provides you with a framework to run distributed systems resiliently. It takes care of scaling and failover for your application, provides deployment patterns, and more. For example, Kubernetes can easily manage a canary deployment for your system.

Kubernetes provides you with:

- Service discovery and load balancing Kubernetes can expose a container using the DNS name or using their own IP address. If traffic to a container is high, Kubernetes is able to load balance and distribute the network traffic so that the deployment is stable.

- Storage orchestration Kubernetes allows you to automatically mount a storage system of your choice, such as local storages, public cloud providers, and more.

- Automated rollouts and rollbacks You can describe the desired state for your deployed containers using Kubernetes, and it can change the actual state to the desired state at a controlled rate. For example, you can automate Kubernetes to create new containers for your deployment, remove existing containers and adopt all their resources to the new container.

- Automatic bin packing You provide Kubernetes with a cluster of nodes that it can use to run containerized tasks. You tell Kubernetes how much CPU and memory (RAM) each container needs. Kubernetes can fit containers onto your nodes to make the best use of your resources.

- Self-healing Kubernetes restarts containers that fail, replaces containers, kills containers that don't respond to your user-defined health check, and doesn't advertise them to clients until they are ready to serve.

- Secret and configuration management Kubernetes lets you store and manage sensitive information, such as passwords, OAuth tokens, and SSH keys. You can deploy and update secrets and application configuration without rebuilding your container images, and without exposing secrets in your stack configuration.

### Advantages of Kubernetes

- Easy organization of service with pods

- It is developed by Google, who bring years of valuable industry experience to the table

- Largest community among container orchestration tools

- Offers a variety of storage options, including on-premises SANs and public clouds

- Adheres to the principals of immutable infrastructure

### Advantages of Docker

- Offers an efficient and easier initial set up
- Integrates and works with existing Docker tools
- Allows you to describe your application lifecycle in detail
- Docker allows the user to track their container versions with ease to examine discrepancies between prior versions
- Simples configuration, interact with Docker Compose
- Docker offers a quick-paced environment that boots up a virtual machine and lets an app run in a virtual environment quickly
- Documentation provides every bit of information
- Provides simple and fast configuration to boost your business
- Ensures that application is isolated

### Disadvantages of Kubernetes

- Migrating to stateless requires many efforts
- Limited functionality according to the availability in the Docker API
- Highly complex Installation/configuration process
- Not compatible existing Docker CLI and Compose tools
- Complicated manual cluster deployment and automatic horizontal scaling set up

### Disadvantages of Docker

- Doesn't provide a storage option
- Has poor monitoring option
- No automatic rescheduling of inactive Nodes
- Complicated automatic horizontal scaling set up
- All the actions have to be performed in CLI
- Basic infrastructure handling
- Manual handling multiple instances
- Need support for other tools for production aspects - monitoring, healing, scaling
- Complicated manual cluster deployment
- No support of health-checks



