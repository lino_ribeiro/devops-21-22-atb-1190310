package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;


import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @org.junit.jupiter.api.Test
    void stringValidation_Empty() {
        //Arrange
        String firstName = "";
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName,"Ribeiro", "Dev",2, "lino@gmail.com"));
    }

    @org.junit.jupiter.api.Test
    void stringValidation_null() {
        //Arrange
        String firstName = null;
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName,"Ribeiro", "Dev",2, "lino@gmail.com"));

    }

    @org.junit.jupiter.api.Test
    void jobYearsValidation_HighestThan100() {
        //Arrange
        int jobYears = 110;
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Lino","Ribeiro", "Dev",jobYears, "lino@gmail.com"));
    }

    @org.junit.jupiter.api.Test
    void jobYearsValidation_LowerThan0() {
        //Arrange
        int jobYears = -1;
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Lino","Ribeiro", "Dev",jobYears, "lino@gmail.com"));
    }

    @org.junit.jupiter.api.Test
    void employeeCreation() {
        //Arrange
        Employee Employee = new Employee("Lino", "Ribeiro", "Dev", 2, "lino@gmail.com");
        Employee EmployeeTwo = new Employee ("Lino", "Ribeiro", "Dev", 2, "lino@gmail.com");

        assertEquals(Employee,EmployeeTwo);
    }

    @org.junit.jupiter.api.Test
    void emailValidation_invalidEmail() {

        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Lino","Ribeiro", "Dev",2, "linogmail.com"));
    }

    @org.junit.jupiter.api.Test
    void emailValidation_invalidEmail_Two() {

        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Lino","Ribeiro", "Dev",2, "lino@gmail.c@om"));
    }



}