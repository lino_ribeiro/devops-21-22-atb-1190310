# DevOps - ca2-part1
---

## Build Tools with Gradle
---
---

### Analyze, design and implementation
---
This is README for the second class assignment.

Gradle is a build automation tool. This type of tools are used to automate the creation of applications.
Gradle combines the best of Ant´s flexibility with dependency management and maven conventions.
Gradle build files are scripts written in Groovy language, otherwise, Ant and Maven build formats use XML files for their configuration.
Since they are based on scripts, Gradle files allow us to do programming tasks in a configuration file. 
Gradle also has a system of plugins that add extra functionality to its core.

---
### Committing to the remote repository (Goal 1)


* First, I start downloading a copy of the files available at: https://bitbucket.org/luisnogueira/gradle_basic_demo/. to a folder that I created, named: **ca2-part1** and commit it to my repository **(Goal 1)**        
  ```` $ git commit -a -m "ca2-part1 initial commit (close#10)"````

### Experimenting the application (Goal 2)

#### Build
  * Build *.jar* file with the application:                
  ````$ ./gradlew build````

#### Run the server
  * Open terminal and execute the following command from the project's root directory:                         
    ```` $ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port> ````
    (The server port should be changed to a valid port, like 59001)

#### Run a client
  * Open another terminal and execute the following gradle task from the project root directory:                     
    ```` $ ./gradlew runClient ````
  * When performing this task, a window will appear, where the use_name will be requested.
    After its introduction, it is possible to start write messages.
  ![Run_Cliente](ChatI.jpg)

  * To better simulate a real chat situation, I opened a new terminal and repeated the command, to represent 2 users who can communicate with each other:
  ![Users_Comunication](ChatII.jpg)

#### Exit application
* To exit application, in the first terminal that you open, press "ctrl+c"

### Run server task (Goal 3)

* The first task that we need to add in the application is a task that runs the server. For that, we have to add the following task to "build.gradle":
```groovy
       task runServer(type: JavaExec, dependsOn: classes) {
         group = "DevOps"
    
         description = "Launches a chat server on localhost:59001"
    
          classpath = sourceSets.main.runtimeClasspath
    
          main = 'basic_demo.ChatServerApp'
         
          args '59001'
       }
```

* After that, start the server through the command:                      
````$ ./gradlew runServer````

![Run_Server](taskRunServer.jpg)

* And then, run the client to open the chat window:             
````$ ./gradlew runClient````

### Add a simple unit test and update the gradle script (Goal 4)
* First, create new directory "AppTest.java" and add following example unit test:        
````groovy
@Test 
public void testAppHasAGreeting() { 
App classUnderTest = new App();
Assert.assertNotNull("app should have a greeting", classUnderTest.getGreeting()); 
}
````
*Then, it´s necessary add *JUnit* dependency in *build.gradle*, so that the gradle script be able to run the test:               
````groovy
dependencies {
// JUnit for testing
implementation 'junit:junit:4.12'
}
````

### Add a simple task of type Copy to be used to make a backup of the sources of the application (Goal 5)
* In *build.gradle* file, add new task type Copy to be used to make a backup of the application sources:            
```` groovy
task backup(type:Copy){
    from "src"
    into "backup"
    }
````

* After that, to run the task, write following command and new backup folder will appear in *gradle_basic_demo*:          
  ````$ ./gradlew runServer````

![Backup](backup.jpg)

### Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application (Goal 6)
* In *build.gradle* file, add new task type Zip to be used to make an archive of the application sources:       
````groovy
task zipFiles(type: Zip) {
    group = "Devops"
    description = "Zip src content"
    from('src/')
    archiveName = 'zipFile.zip'
    destinationDirectory= file('backup')
}
````
* After that, to run the task, close the other terminals that were open, open a new one and apply the zipFiles:            
````$ ./gradlew zipFiles````

![ZipFile](ZipFile.jpg.png)

* Finally, it was added README.md and marked repository with the tag *ca2-part1*


