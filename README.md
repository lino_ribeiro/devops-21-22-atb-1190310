# Individual Repository for DevOps

This repository contains the files for all the class assignments of DevOps.

* [Class Assignment 1](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca1/README.md)

* [Class Assignment 2 - Part1](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca2/ca2-part1/README.md)

* [Class Assignment 2 - Part2](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca2/ca2-part2/README.md)

* [Class Assignment 3 - Part1](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca3/ca3-part1/README.md)

* [Class Assignment 3 - Part2](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca3/ca3-part2/README.md)

* [Class Assignment 4 - Part1](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca4/ca4-part1/README.md)

* [Class Assignment 4 - Part2](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca4/ca4-part2/README.md)

* [Class Assignment 5 - Part1](https://bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310/src/master/ca5/ca5-part1/README.md)

* [Class Assignment 5 - Part2]()
