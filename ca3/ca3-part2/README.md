# DevOps - ca3-part2

## 1. Virtualization with Vagrant

This is the README for the third class assignment.

This class assignment subject is **Virtualization with Vagrant**.

On this second part of the third assignment, the goal is to setup a virtual environment to execute the tutorial spring boot
application, gradle "basic" version (developed in CA2, Part2).

### Introduction to Vagrant

Vagrant is an application that creates and configures virtual development environments.
It can be used as a simpler solution around virtualization software (like VirtualBox, VMware, KVM or Linux Containers (LXC)),  
and configuration management software (like Ansible, Chef, Salt or Puppet).
The most used virtual machine platform on Vagrant is Virtualbox (the one that we use) which is developed by Oracle.
It's cross-platform and can run a large number of operating systems easily, allowing to freely limit, for example, the amount of RAM, CPU and disk.
In the case of Vagrant, you can, with a few commands, start, provision, and configure Virtualbox so that you can use
the virtual machine in an intuitive way.

## Get the Vagrantfile (Issue 1)

After create a new directory to this task (ca3-part2), it is necessary add a copy of ca2-part2 directory inside this;

Then, you should copy the **Vagrantfile** that is on https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ to the *ca3-part2* directory and push it:      
````$ git status````  
````$ git add .````  
````$ git commit -m "Create new directory and add files from task Ca2-part2 (close#42)````  
````$ git push````

## Study the Vagrantfile (Issue 2)

Go to ca3-part2 and execute **VagrantFile** to provision and start two VMs, one that is going to work as a database (for persistence), and other to run the Spring application:
````$ vagrant up````

### Common provision analysis

```Ruby
config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
SHELL
```

The previous provision is common for both VMs, it will install IP utils, Avahi Daemon, Unzip and JDK.

### Database provision analysis

```Ruby
config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.56.11"
```

This box will install Ubuntu 16.04.7 LTS (Xenial Xerus).

The hostname will be "db".

The IP address configured for this VM will be "192.168.56.11". The database port will be "9092".


```Ruby
db.vm.network "forwarded_port", guest: 8082, host: 8082
db.vm.network "forwarded_port", guest: 9092, host: 9092
```

Here, we forwarded the guest port "8082" to the host port "8082" and the guest port "9092" to the host port "9092".

We did that because we want to access H2 console from the host using port "8082" and to connect to the H2 server using port "9092"

Now we want the H2 database to be installed only in the first time the "vagrant up" command is used. We can do it with the following provision:

```Ruby
db.vm.provision "shell", inline: <<-SHELL
    wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
SHELL
```

However, the following provision shell will always run, in order to execute the H2 server process, using java. This is achieved by applying the "always" flag.

```Ruby
db.vm.provision "shell", :run => 'always', inline: <<-SHELL
    java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
SHELL
```

### Web server provision analysis

```Ruby
config.vm.define "web" do |web|
	web.vm.box = "bento/ubuntu-18.04"
	web.vm.hostname = "web"
	web.vm.network "private_network", ip: "192.168.56.1"
```

This box will install Ubuntu 16.04.7 LTS (Xenial Xerus).

The hostname will be "web".

The IP address configured for this VM will be "192.168.56.1".

```Ruby
web.vm.provider "virtualbox" do |v|
	v.memory = 1024
end
```

Here we instructed the virtualization software to allocate "1024" megabytes (1GB)s of memory to this virtual machine.

```Ruby
web.vm.network "forwarded_port", guest: 8080, host: 8080
```

Here, we forwarded the guest port "8080" to the host port "8080".

We did that because we want to access tomcat from the host using port "8080".

```Ruby
web.vm.provision "shell", inline: <<-SHELL, privileged: false
	sudo apt-get install git -y
	sudo apt-get install nodejs -y
	sudo apt-get install npm -y
	sudo ln -s /usr/bin/nodejs /usr/bin/node
	sudo apt install tomcat8 -y
	sudo apt install tomcat8-admin -y
	# If you want to access Tomcat admin web page do the following:
	# Edit /etc/tomcat8/tomcat-users.xml
	# uncomment tomcat-users and add manager-gui to tomcat user

	# Change the following command to clone your own repository!
	git clone https://atb@bitbucket.org/atb/tut-basic-gradle.git
	cd tut-basic-gradle
	chmod u+x gradlew
	./gradlew clean build
	# To deploy the war file to tomcat8 do the following command:
	sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
SHELL
```

The previous provision will install git, nodejs, npm and tomcat8.

After that it will clone a git repository (in this case https://atb@bitbucket.org/atb/tut-basic-gradle.git).

Then it will change its directory to "tut-basic-gradle" and give all permissions to "gradlew" in the current user.

It will then run "./gradlew clean build" to make a clean build and copy the resulting "war" file from "./build/libs/basic-0.0.1-SNAPSHOT.war" to "/var/lib/tomcat8/webapps".


## Update the Vagrantfile configuration (Issue 3)

You need to update the Vagrantfile configuration in order to make it compatible with your project and repository.

Simply change the lines 70, 71 and 75 of the Vagrantfile to clone your own repository, change the VM directory to the right place and match the name of the built "war" file.

````ruby
# Change the following command to clone your own repository!
      git clone https://lino_ribeiro@bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310.git
      cd devops-21-22-atb-1190310/ca2/ca2-part2/react-and-spring-data-rest-basic
      chmod u+x gradlew
      ./gradlew clean build
      ./gradlew bootWar
      # To deploy the war file to tomcat8 do the following command:
      sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar /var/lib/tomcat8/webapps
    SHELL
````


## Changed the tut-basic-gradle application (Issue 4)
### build.gradle changes
(The project was updated, since this issue)

First, in the "build.gradle" file, under the "plugins", add:

```
id 'war'
```

After that, in the "build.gradle" file, under "dependencies", add:

```
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
```

Finally, you have to change the java version in your "build.gradle" file to 11, by adding the following lines:

```
sourceCompatibility = '11'
```


### ServletInitializer.java creation

Then, create the "ServletInitializer.java" class.

It should have the following content:

```
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}
```

### application.properties changes

After that, insert the following lines to "src/main/resources/application.properties" file to enable support for the H2 console:

```
server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

### app.js changes

Don't forget to change the application context path to match the project build. You can do it in "src/main/js/app.js", like this:

```
componentDidMount() { // <2>
		client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
}
```

### index.html changes

Then you need to remove the backslash on line 6 of the "main/resources/templates/index.html" file.


### Build and commit

You can now build your project and commit the changes to your repository.


## Replicate the checked changes (Issue 5) and VM implementation process (Issue 6)

### Repository privacy

You need to make your remote repository public, like this:


### Change .gitignore

````
# Created by https://www.gitignore.io/api/gradle
# Edit at https://www.gitignore.io/?templates=gradle

### Gradle ###
.gradle
build/

# Ignore Gradle GUI config
gradle-app.setting

# Avoid ignoring Gradle wrapper jar file (.jar files are usually ignored)
!gradle-wrapper.jar

# Cache of project
.gradletasknamecache

# # Work around https://youtrack.jetbrains.com/issue/IDEA-116898
# gradle/wrapper/gradle-wrapper.properties

### Gradle Patch ###
**/build/

# End of https://www.gitignore.io/api/gradle

*.iml
.idea
target
bower_components
node_modules
node
*.log
asciidoctor.css
README.html
built
dump.rdb
package-lock.json
# Created by https://www.toptal.com/developers/gitignore/api/eclipse
# Edit at https://www.toptal.com/developers/gitignore?templates=eclipse

### Eclipse ###
.metadata
bin/
tmp/
*.tmp
*.bak
*.swp
*~.nib
local.properties
.settings/
.loadpath
.recommenders

# External tool builders
.externalToolBuilders/

# Locally stored "Eclipse launch configurations"
*.launch

# PyDev specific (Python IDE for Eclipse)
*.pydevproject

# CDT-specific (C/C++ Development Tooling)
.cproject

# CDT- autotools
.autotools

# Java annotation processor (APT)
.factorypath

# PDT-specific (PHP Development Tools)
.buildpath

# sbteclipse plugin
.target

# Tern plugin
.tern-project

# TeXlipse plugin
.texlipse

# STS (Spring Tool Suite)
.springBeans

# Code Recommenders
.recommenders/

# Annotation Processing
.apt_generated/
.apt_generated_test/

# Scala IDE specific (Scala & Java development for Eclipse)
.cache-main
.scala_dependencies
.worksheet

# Uncomment this line if you wish to ignore the project description file.
# Typically, this file would be tracked if it contains build/dependency configurations:
.project
.classpath

### Eclipse Patch ###
# Spring Boot Tooling
.sts4-cache/

# End of https://www.toptal.com/developers/gitignore/api/eclipse
````


### Create and configure the guest machines according to the Vagrantfile

Finally, in order to replicate the checked changes in Issue 4, just run the following command in the directory where you have your Vagrantfile:

```bash
vagrant up
```

Now, you can go to the follow link in order to see your website:

http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT

## Mark your repository with the tag ca3-part2

After finalizing the assignment and completing the README file, the repository should be market with a new tag "ca3-part2".

We can do this using the following **Git** commands:

```
git tag -a ca3-part2 -m "End of ca3-part2 assignment"

git push origin ca3-part2
````


## 2. Analysis of an Alternative

- Hyper-V and Oracle VM VirtualBox can both be used to handle a businesses' server virtualization needs, but they also have a few features that set them apart from each other.


- Hyper-V offers high performance virtual machines, and can output a lot of power depending on the hardware that it is running on.  Additionally, since Hyper-V is a type 1 hypervisor, virtual machines are always running as long as the hardware is.  Hyper-V also integrates well with Windows infrastructures, and is simple to use once it has been implemented.


- Oracle VM VirtualBox can run on several operating systems, including Windows, Linux, and MacOS.  In addition to running on multiple host operating systems, Oracle VM VirutalBox can also create VirtualMachines using multiple guest operating systems, rather than just Windows. Lastly, implementation of Oracle VM VirtualBox is simple, most businesses will be able to just install the hypervisor and be ready to go.


- Hyper-V and Oracle VM VirtualBox both help businesses to handle virtualization, but they also have a few limitations that are important to consider.


- Hyper-V is simple to use once it’s implemented, as the virtual machines run as long as the hardware is running, but it isn’t as easy to set up as Oracle VM VirtualBox, which is a simple installation for most users.  Additionally, while Hyper-V is great for businesses using Windows host systems that want to run Windows servers, Hyper-V can’t handle non-Windows operating systems.  Businesses looking for a server virtualization tool that can run on non-Windows host operating systems should consider other options.


- Oracle VM VirtualBox is simple to implement, but is more difficult to manage compared to Hyper-V, which runs as long as the hardware is running.  Additionally, Oracle VM VirtualBox can’t create virtual machines with the same performance as Oracle VM VirtualBox.  For businesses looking for high performance virtual machines that have the hardware to support them, Hyper-V may be a better option.


## 3. Implementation of Alternative

### Copy the Vagrantfile

Create a folder named ca3-part2-alternative and copy the Vagrantfile from ca3-part2 to this new folder.


### Update the Vagrantfile

Open the Vagrantfile and change the lines that concerns to configuration of the VM box.

- Replace "bento/ubuntu-18.04" " for "hashicorp / bionic64";

- Add this configuration, so that it will not require password when you initiaize the VMs with Hyper-V:
```
config.vm.synced_folder '.', '/vagrant', disabled: true
```

### Initialize the VMs

To initialize the VMs using Hyper-V as provider:
```
vagrant up --provider hyperv
```

### Make some changes in the VM web

#### Enter the VM web:
```
vagrant ssh web
```

#### Enter your repository in the VM:
```
cd devops-21-22-atb-1190310\ca2\ca2-part2\react-and-spring-data-rest-basic\src\main\resources
```

#### Enter the application.properties:
```
nano application.properties
```

#### Edit the application.properties, so it looks like this:
```
server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
#spring.datasource.url=jdbc:h2:tcp://192.168.105.163:9092/~/jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.105.163:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

Note:
To find the IP you should use in application.properties, open the Hyper-V Manager and check the IP in the VM db.


#### Exit the VM web:
```
exit
```

#### To apply the changes you've made and reload the VM:
```
vagrant reload --provision web
```

### Check the frontend

To check if the frontend of your project is working, open your browser and enter:
```
http://192.168.102.75:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
```

Note:

To find the IP you should use in the browser to open the frontend, open the Hyper-V Manager and check the IP in the VM web:


### Check the database

To check if the database of your project is working, open your browser and enter:
```
http://192.168.102.75:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
```

Note:

The IP you should use in the browser to open the database is the same as the one you checked in the Hyper-V Manager.


### Shut down the VMs

Now that you achieved to execute a Gradle project in your VMs, you can shut down the VMs with the command:
```
vagrant halt
```


### Note:
To do this assignment, I needed to repeat the same process many times. For that, each commit, contains many changes.