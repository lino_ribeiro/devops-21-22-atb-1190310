# DevOps - ca5-part1
---

## CI/CD Pipelines with Jenkins
---
---
## 1. Analysis, Design and Implementation

* First of all, I would like to point out that CI means Continuous Integration and CD, Continuous Delivery;

* Continuous Integration is the most important part of DevOps or at least the most precognized. This component is responsible for the integration of the various DevOps stages.  
  Continuous Integration is a development practice in which the developers are required to commit changes to the source code in a shared repository
  several times a day or more frequently. Every commit made to the repository is then built automatically [1].

* Jenkins is the most famous Continuous Integration tool;

![Jenkins](Jenkins.jpg)

* Jenkins is an open source automation tool written in Java with plugins purpose built for Continuous Integration.
  Jenkins is used to build and test software projects continuously, making it easier for developers to integrate changes to the project,
  and making it easier for users to obtain a fresh build.
  It also enables the continuous delivery of software by integrating with a large number of testing and deployment technologies.

* So its possible to make a before and after Jenkins, where the main differences are [2]:
    * **Before Jenkins**:
        * The entire source code was built and then tested. Locating and fixing bugs in the event of build and test failure
          was difficult and time consuming, which in turn slows down the software delivery process;
        * Developers have to wait for test results;
        * The whole process is manual;
    * **After Jenkins**:
        * Every commit made to the source code is built and tested. So, instead of checking the entire source code, developers
          only need to focus on a particular commit. This leads to frequent new software releases;
        * Developers know the test results of every commit made in the source code;
        * We only need to commit changes to the source code. Jenkins will then automate the remaing processes for us;  

#### Jenkins Installation:
* Go to the official site and install the Jenkins War file, [Jenkins_War](https://www.jenkins.io/doc/book/installing/);

* Download the latest stable Jenkins war;


* Put the war executable on the directory where I intend to run the jar command;  
  ````$ java -jar jenkins.war --httpPort=9090````  
  I chose to change the port to avoid future conflicts with other projects that use the 8080 port;


* Take the password that appears in PowerShell, and paste it to the browser;   

* Install suggested plugins;  
  They help extend Jenkins capabilities and integrated Jenkins with other software.

* Start using Jenkins, defining username, password, name and email;  

#### Create a new Job:
* The new process that will be executed for the project:

![JenkinsStartingPage](Welcome_Jenkins.JPG)


* Define the Job name: **devops1190310**;

* Choose the pipeline type, in this case **declarative**:
    * Declarative Pipeline syntax offers a simple, predefined hierarchy to make the creation of pipelines and the associated
      Jenkinsfiles accessible to users of all experience levels.

* Then I start configuring my job;  
  It's possible to define several components like, the building triggers, some parameterizations, but the only thing that I changed
  was the pipeline;

I will use a Script from SCM, which means that I have a Jenkinsfile (the script that defines the pipeline) in the root of my project, **Ca2-Parte1**, where I
will define all the Job stages.  

#### Configure Pipeline on the browser:

* I have to create the Pipeline definition:  
  ````Pipeline script from SCM````

* Define the SCM as Git;

* In my case I didn´t add credentials because I have a public repository;

* Define the repository URL:  
  ````https://lino_ribeiro@bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310````

* Define the Script Path:  
  ````Ca2-parte1/gradle_basic_demo/Jenkinsfile````  


#### Configure Jenkinsfile

```shell

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://lino_ribeiro@bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310.git'
            }
        }
        
        stage('Assemble') {
            steps {
				dir('ca2/ca2_part1/gradle_basic_demo/') {
					echo 'Assembling...'
					script{
						if (isUnix()){
							sh 'chmod +x ./gradlew'
							sh './gradlew clean assemble'
						}
						else{
							bat './gradlew clean assemble'
						}
					}
                }
            }      
        }
		
		stage('Archiving') {
            steps {
				dir('ca2/ca2_part1/gradle_basic_demo/') {
					echo 'Archiving...'
                    archiveArtifacts 'build/libs/*'
				}
            }     
        }
        
        stage('Test') {
            steps {
				dir('ca2/ca2_part1/gradle_basic_demo/') {
                    echo 'Testing...'
					script{
						if (isUnix()){
							sh './gradlew clean test'
							junit '**/TEST-basic_demo.AppTest.xml'
						}
						else{
							bat './gradlew clean test'
							junit '**/TEST-basic_demo.AppTest.xml'
						}
					}
                }
            }     
        }
        

    }
}

```

This jenkinsfile should be placed where you declared your pipeline jenkinsfile path.

This will create 4 stages:
1. Checkout - Will checkout to your git repository using the given credentials;
2. Assemble - Will assemble a clean build of the project;
3. Archiving - Will archive the generated artifact;
4. Test - Will execute clean tests and save its report.


#### Building the Job  

Now you can run your pipeline by entering it and clicking "Build now" in the sidebar.

NOTE: The first time I run "Build now" was wrong because the path contained an error. In second time, it built.

The results will be shown to you like in the printscreen below.

![Build_Success](Built.JPG)


### Mark your repository with the tag ca5-part1

After finishing the assignment and completing the README file, the repository should be market with a new tag "ca5-part1".

We can do this using the following **Git** commands:

```shell
git tag -a ca5-part1 -m "End of ca5-part1 assignment"

git push origin ca5-part1
