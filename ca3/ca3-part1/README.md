# DevOps - ca3-part1
---

## Virtualization with Vagrant
---
---

On this first part of the third assignment, the goal is to practice with VirtualBox using the same projects from the previous assignments but now inside a
VirtualBox VM with Ubuntu.

* By virtualizing our server, we can place many virtual servers into each physical server to improve hardware utilization;
* We don't need to purchase additional physical resources, like hard drives or hard disks;
* A VM provides an environment that is isolated from the rest of a system, so whatever is running inside a VM won’t interfere with anything else running on the host hardware;
* Since VMs are isolated, they are a good option for testing new applications or setting up a production environment.

### 1. Analyze, design and implementation

### Virtualization

Virtualization is the process of running a virtual instance of a computer system in a layer abstracted from the actual hardware.

### Hypervisor

A hypervisor is a computer software, firmware or hardware that creates and runs virtual machines.

A computer on which hypervisor runs one or more virtual machines is called a **host machine**, and each virtual machine is called a **guest machine**.

Hypervisors can be divided into two types:

- Type 1, native or bare-metal hypervisors: These run directly on the host computer’s hardware to control the hardware resources and to manage guest operating systems.

- Type 2, hosted hypervisors: These run within a conventional operating system environment.  A guest operating system runs as a process on the host. This type also abstracts guest operating systems from the host operating system.

The following image illustrates those two types:

![Hypervisor](Hypervisor.jpg)

The chosen hypervisor for the assignment was **VirtualBox** which is a type 2 hypervisor.

## Create ca3-part1 folder

* Go to our repository:  
  ```` cd C:\git\repos\devops-21-22-atb-1190310````
    * create a directory to the new task  
      ```` mkdir ca3-part1````
    * create a README file  
      ```` echo README >> README.md````
    * confirm that I have changes to commit and add them to the *Staging Area*  
      ```` git status````
      ```` git add ca3-part1````
    * commit all changes and push them to the remote repository  
      ```` git commit -m "create ca3-part1 folder (close#20)"````  
      ```` git push````

## Download VirtualBox (Issue 1)

You can download the latest VirtualBox version here:

https://www.virtualbox.org/

## Configure the Virtual Machine with Ubuntu (Issue 2)

To be able to access the guest OS, it's necessary to configure the network interface of the machine.
We should connect the (image)[https://help.ubuntu.com/community/Installation/MinimalCD] (ISO) with the Ubuntu 18.04 minimal installation media.
After that, we should set up the follow characteristics:

- The VM needs 2048 MB RAM.
- Set Network Adapter 1 as Nat
- Set Network Adapter 2 as Host-only Adapter.

To do the latest, we need to add to the guest a `Host-only Adapter`. To do that:

- From main menu select File -> Host Network Manager
- Click create button. A new Host-only network will be created and added to the list.

We have now enabled Network Adapter 2 as Host-only Adapter.

We should check the IP address range of this network.

In this case it was 192.168.56.1/24.


## Run your Virtual Machine (Issue 3)

To run your virtual machine just press the "Start" button:

![Virtual Machine](VM.jpg)


## Update Ubuntu's applications (Issue 4)

You can now continue the setup.

First, update the packages repositories:

```
sudo apt update
```

Then, install the network tools:

```
sudo apt install net-tools
```

Now, edit the network configuration file to set up the IP:

```
sudo nano /etc/netplan/01-netcfg.yaml
```

![IPSetup](01-netcfg.yaml.jpg)

## Install Git (Issue 5)

To install **Git** you can enter the following command:

```
sudo apt install git
```

## Install JDK (Issue 6)

To install **JDK** you can enter the following command:

```
sudo apt install openjdk-8-jdk-headless
```

## Install Gradle (Issue 7)

To install **Gradle** you can enter the following command:

```
sudo apt install gradle
```
## Install Maven (Issue 8)

```
sudo apt install maven
```

## Clone repository with spring boot tutorial basic project (ca1) and the gradle tutorial basic (ca2-part1) (Issue 9)

To clone the repository which contains both projects, insert the following command inside the directory that you want the repository to be cloned to:

```
git clone https://lino_ribeiro@bitbucket.org/lino_ribeiro/devops-21-22-atb-1190310.git
```

## Build and execute the spring boot tutorial basic project (Issue 10)

Open the **basic** directory, where you have the spring boot tutorial basic project with the changes that you made:     
```` cd ca1\basic````   

When you tried to make the spring-boot run, maybe that won't be possible, because of some permissions issues, namely with the *mvnw* file, so you should to give execute permission:         
```` chmod +x mvnw````  

Lastly, try to execute again the basic spring boot tutorial:    
```` ./mvnw spring-boot:run````

Then, open the application using the host computer's browser, and the URL:      
```` http://192.168.56.5:8080/````
 
Everything went has expected, as It's possible to see in the following picture:      

![Spring](Spring.jpg)

![Basic_8080](8080.jpg)

## Build and execute the gradle tutorial basic (Issue 11)

Next, change to the directory where you have the Gradle project:          
```` cd devops-21-22-atb-1190310/ca2-parte2````

List your files and directories to check permissions, and once again you need to change the permission of the gradlew file:        
```` chmod +x gradlew````

Execute the gradle project:           
```` ./gradlew build````

And run the server in your VM:             
```` ./gradlew runServer```` 

Now, in order to test the client part of the project, you need to run the client applications in our host OS, since Ubuntu does not have a Graphical Unit Interface.

Since the server is now being hosted in the Virtual Machine, you need to modify the runServer task for the client of the application in order to execute said task and connect to the server IP instead of the localhost.

In order to do so, we need to edit the following task in your "build.gradle" inside our repository located in our host OS. So you need to go there:


```groovy
task runClient(type:JavaExec, dependsOn: classes){
group = "DevOps"
description = "Launches a chat client that connects to a server on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
````

Then you need to build the application using:

```
gradle build
```

Finally, you can run the client application.


## At the end of the part 1 of this assignment mark the repository with the tag ca3-part1

With the conclusion of this class assignment task, you can now add the "ca3-part1" tag to your repository.

In order to do that you can run the following commands in your local repository:

```
git tag -a ca3-part1 -m "End of ca3-part1 assignment"

git push origin ca3-part1
```
