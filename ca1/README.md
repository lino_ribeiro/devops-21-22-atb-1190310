# Relatório DevOps - ca1

## Demonstração de um cenário de Git Workflow
---

---
### Descrição da análise, *design* e implementação de requisitos
---

Com esta primeira tarefa, ca1, pretendia-se simular um cenário de *Git Workflow*, num ambiente de trabalho em que seria possível a criação de diferentes *branches*, partilhando o mesmo repositório remoto por vários *developers*.

O repositório a ser utilizado foi clonado e foi guardado numa pasta conforme a indicação com o nome de: <devops-21-22-atb-1190310>. As pequenas alterações criadas na primeira aula foram eliminadas e foi criada a pasta ca1 onde consta todo o trabalho desenvolvido para a primeira *class assignment*.
Aquando da realização do clone, foi também criado um arquivo *.gitignore*, que lista aquilo que o *git* deve ignorar.
As alterações foram efetuadas num único ficheiro: "basic".
Foram analisadas quais as necessidades do cliente e as *features* a serem desenvolvidas para responder às mesmas. De modo geral o trabalho foi realizado, seguindo os seguintes passos:

1. Análise dos requisitos do cliente;
2. Definição de novas *features* a serem desenvolvidas;
3. Criação de Issues para assegurar a concretização das tarefas (realização de diversos commits a referenciar as respetivas issues);
4. Formação de *branches* para responder a cada uma das *features*;
5. Merge desses *branches* com o *master*, quando já foi desenvolvida uma resposta efetiva;
6. Criação de tags para representar as diferentes versões quando estabilizadas;

* Instalação do Git (este passo não foi necessário, visto que já tinha sido previamente instalado) [Git](https://git-scm.com/downloads);


* Após instalação podemos confirmar que tudo correu como esperado, utilizando o comando que permite determinar a versão que se encontra instalada:     
````$ git --version````

* Após criação da pasta, foi iniciado o repositório, recorrendo ao comando:     
````$ git init 'c:/git/repos/devops-21-22-atb-1190310/````

* Mudar de diretório na linha de comandos para o diretório onde se encontra a pasta ca1:    
````cd c:/git/repos/devops-21-22-atb-1190310/ca1/````

* Inicialmente todos os *commits* foram feitos no *Master Branch*, confirmando o mesmo através do comando:   
````$ git branch````

* Confirmar quais os ficheiros que estão a aguardar para serem adicionados à *Staging Area*, e os ficheiros *Untracked*:  
````$ git status````
                                
* Adicionar esses ficheiros à *Staging Area* e fazer *Commit* dos mesmos. Os *commits* podem ter uma mensagem e referenciar a *Issue*, bastando adicionar a expressão "close #1 (nr da issue)". Neste *commit* foi incluinda toda a documentação necessária para a realização do trabalho (tut-react-and-spring-data-rest):    
````$ git commit -a -m "Create ca1 folder"````  
  (A pasta foi criada recorrendo ao comando `mkdir ca1`)

Quando fazemos *commit* podemos também optar por não colocar o comentário do mesmo, imediatamente na linha de comando, fazendo apenas:   
````git commit````  
O que irá abrir uma janela, onde deverá ser adicionada a mensagem, contudo achei mais vantajoso usar a primeira alternativa apresentada.    

* A primeira alteração realizada, foi a adição de um ficheiro README.md, seguindo os seguintes passos:    
    * criação do mesmo na linha de comandos:      
      ````echo 'readme.file' >> README.md````
    * em seguida foi adicionado à *Staging Area*, feito o *commit* e *push*:      
      ````git commit -a -m 'Add README file close #1'````  
      ````git push ````
    * posteriormente foi atribuído um tag de versão inicial a esse mesmo *commit*, usando o comentário do *commit*:     
        * confirmação do comentário do *commit*:      
        ````git log````
        * criação da *tag*:      
        ````git tag v1.1.0 da771f4````
        * fazer a replicação da *tag* para o repositório remoto:       
        ````git push --tags````
        * Se for necessário apagar a tag do repositório local, usar o comando:     
        ```$ git tag -d v1.2.0```
        * Para apagar a tag do repositório remoto:     
        ````$ git push --delete origin v1.2.0````

* Criação de uma nova *feature*, num novo branch, chamado **email-field**:      
````git branch email-field````
  * Passar a trabalhar nesse *branch*:        
  ````git checkout email-field````
  * Confirmar que estamos no *branch* correto:           
  ````git branch````
  * Após todas as alterações necessárias para a implementação da *feature* pretendida, foi realizado *commit* e *push* das mesmas:      
    ````git commit -a -m "Add email field, tests and attribute validation (close#4,#5 and #6)"````
    ````git push origin email-field````
    (este *commit* fechou 3 issues visto que, ao criar um novo campo no construtor de Employee, teria de corrigir todos os testes que utilizavam o construtor, caso contrario os testes não passavam. De forma a evitar enviar para o repositório remoto testes que não passavam, foram alterados imediatamente e enviado tudo no mesmo commit);
  * Merge dos branch's:    
    * Passar para o *master branch*:     
      ````git checkout master````
    * Realizar o *merge* do *master branch* com o *email-field branch*:     
      ````git merge email-field````
    * Realizar o *push* de todas as alterações efetuadas:     
      ````git push origin master````
    * Por fim, foi criada uma tag (no *master branch*) para a nova versão:     
      ````git tag v1.3.0 6d7ba7e````
  
* Criação de um novo *branch* para efetuar correção de eventuais *bugs* no campo *email*, com o nome **fix-invalid-email**:  
  * Criar o *branch* e, em simultâneo, mover o *head* para o branch recém-criado:     
    ````git checkout -b fix-invalid-email````
  * Confirmar que estamos no *branch* correto:           
    ````git branch````
  * Após terem sido realizadas as alterações de forma a implementar a *feature* corretamente, foi feito *commit* e *push* das mesmas:      
    ````git commit -a -m "Correct bug in email field validations(close#7 and #8)"````
    ````git push origin fix-invalid-email````
  * Realizaram-se os testes para comprovar a implementação da *feature*:        
    ````git commit -a -m "Test new email validations"````
    ````git push origin fix-invalid-email````
  * Alterar o *head* para o *master branch*:     
    ````git checkout master````
  * Atualização de eventuais alterações:     
    ````git pull origin master````
  * Foram realizadas pequenas alterações no *master branch* (ver explicação abaixo*) e feito o respetivo *commit*:      
    ````git commit -a -m "Add new employees"````
  * Realizar o *merge* do *fix-invalid-email* com o *master*:    
    ````git merge fix-invalid-email````
  * Realizar o *push* de todas as alterações após resolução dos conflitos:       
    ````git push origin master````
  * Por fim, foi criada uma *tag* para esta nova versão:     
    ````git tag v1.3.1 f781fbc````    
    ```` git push --tags````
  
###NOTA
Aquando a realização do *merge* do primeiro *branch* (*email-field*) com o *master*, optei pela realização de um *merge fast forward* em que, não existindo nenhum *commit* no branch *master* posterior à criação do *branch* *email-field*, existe apenas uma alteração do apontador desde o último *commit* do *branch email-field* para o *branch master*. Nesta situação, não é visível, no *Bitbucket* a representação gráfica do *merge*;

Por outro lado, aquando a realização do *merge* do segundo *branch* (*fix-invalid-email*), de forma a poder visualizar graficamente o *merge* no *Bitbucket*, optei pela realização de um *merge three way* em que o último *commit* do *master* é posterior à criação do *branch email-field*. Conforme foi dito anteriormente, apenas foi seguida esta metodologia para poder visualizar graficamente o no *Bitbucket*, estando ciente que não será a metodologia mais adequada visto que, quando trabalhamos em *branches* com equipas numerosas, o *master branch* deve apenas refletir os *commits* concluídos provenientes dos *branches* secundários. Isto deve-se ao facto de o *master branch* ser o *branch* que está à ser integrado no *jenkins*, por exemplo.


* Após os *branches* terem sido utilizados e já não serem mais úteis no futuro, podem ser eliminados. Para isso efetuamos os seguintes passos:
  * Confirmação dos *branches* que já foram *merged*:     
   ````git branch --merged````
  * Eliminação dos diferentes *branches*
    * Eliminação do *branch* local:   
    ````git branch -d email-field````
    * Eliminação do *branch* remoto                                              
    ````git push origin --delete email-field````
    * Eliminação do *branch* local                                                                                     
    ````git branch -d fix-invalid-email````
    * Eliminação do *branch* remoto                                                                                                                        
    ````git push origin --delete fix-invalid-email````

No final, esta é a imagem geral no Bitbucket, ainda sem a adição da *tag* "ca1-part2", pois apenas será introduzida após a finalização do ficheiro README.md
![Bitbucket - Repositório Remoto](
Bitbucket.jpg)



### Fossil como alternativa ao GIT
---
### Fossil como alternativa ao GIT
___
Fossil e Git sobrepõem-se de várias maneiras. Ambos são sistemas de controlo de versão distribuídos que armazenam uma árvore de objetos de *check-in* num clone de repositório local. Em ambos os sistemas, o clone local começa como uma cópia completa do repositório. Novo conteúdo é adicionado ao clone local e, posteriormente, opcionalmente enviado ao repositório remoto, e as alterações no repositório remoto podem ser baixadas para o clone local. Ambos os sistemas oferecem comparação de versões, aplicação de patches, *branching*, *merging*, *branchs* privadas, etc.

O Fossil demarca-se quando se pretende um sistema distribuído de controlo de versões com uma interface de linha de comandos simples e intuitivo, em equipas com uma dimensão relativamente reduzida.

O Git, por outro lado, é um sistema com uma adoção muito maior, o que implica existir um manancial de aplicações "third-party" (Github, Gitlab, Bitbucket, ....) com as quais pode interagir. Este permite também um completo controlo do histórico de alterações, podendo mesmo eliminar quaisquer erros que se possam ter introduzido (no caso do Fossil todas as alterações são imutáveis).

Resumindo, o GIT é um produto maduro que é utilizado por milhões de programadores e projetos de *software* que permite uma colaboração praticamente infinita.

O Fossil é uma proposta mais simples e de nicho que, no entanto, incorpora grande parte das funcionalidades necessárias ao processo de desenvolvimento de *software*, com garantias de que assenta numa tecnologia com provas dadas relativamente a robustez e fiabilidade.



### Implementação do Fossil
___
* Para trabalhar com Fossil, é necessário primeiro fazer a sua instalação, para isso:
    * Abrir a página e fazer o *download* para Windows de: [Fossil SCM](https://www.fossil-scm.org/home/uv/download.html)

* Guardar o executável numa pasta, e colocar esta na *PATH* (variáveis de ambiente do Windows):  
  ````C:\git\repos\repos.fossil````

* Daí em diante é possível executar o Fossil a partir da linha de comandos, como a *PowerShell*, e podemos confirmar a versão instalada:  
  ````fossil version````

* É possível iniciar um novo projeto ou fazer um clone de um repositório remoto, neste caso optei por criar um novo repositório local:  
  ````fossil init repos.fossil````

* Para fazer com que a pasta local possa estar ao abrigo do controlo de versões Fossil:  
  ````fossil open repos.fossil ````

* Adicionar todos os ficheiros ao controle de versões:  
  ````fossil add .````

* Para sabermos o estado do repositório local:  
  ````fossil status````

* Para fazer commit é possível através de:       
  ````fossil commit```` (que abre uma janela onde se escreve à *posteriori* a mensagem do *commit*) 

* Em contrapartida, podemos criar a mensagem imediatamente no *commit*      
  ````fossil commit -m "initial commit"````

* Para criarmos *tags* num determinado commit:      
  ````fossil tag add v1.1.0  a867eaae7d ````
  ````fossil push````


* Para a criação de branches para responder às features:    
````fossil branch new email-field trunk````

* Para começar a fazer as alterações nesse branch:    
````fossil checkout email-field````

Posteriormente foram feitas as alterações no IDE para implementar as *features*

  * Confirmar se existem alterações:  
  ````fossil status````

  * Após a *feature* ser cumprida faz-se o *commit*:  
  ````fossil commit -m "add email-field" ````

  *Passar para o *trunk branch*:      
   ````fossil checkout trunk````

  * Fazer um update para o ramo principal (trunk)- correspondente ao pull do Git:  
  ````fossil update````

  * Fazer o merge do email-field com o trunk:                
   ````fossil merge email-field````

  * Finalmente fazer o push:  
  ````fossil push````

E repetir para o fix-invalid-email:

  * Para a criação de *branches* para responder às *features*:                         
  ````fossil branch new fix-invalid-email trunk````

  * Para começar a fazer as alterações nesse *branch*:  
  ````fossil checkout fix-invalid-email````

  * Confirmar se existem alterações:  
  ````fossil status````

Posteriormente foram feitas as alterações no IDE para implementar as *features*

  * Após a *feature* ser cumprida faz-se o *commit*:  
  ````fossil commit -m "fix-invalid-email" ````

  * Passar para o *trunk branch*:      
    ````fossil checkout trunk````

  * Fazer um update para o ramo principal (trunk)- correspondente ao pull do Git:  
  ````fossil update````

  * Fazer o merge do email-field com o trunk:             
  ````fossil merge fix-invalid-email````

  * Finalmente fazer o push:  
  ````fossil push````

  
Também aqui é possível criar *issues*, denominadas de *tickets*. No contexto deste trabalho não foram criadas, visto que se trata apenas uma apresentação de alternativa e seria duplicar todo o trabalho previamente desenvolvido no *Bitbucket*. Ainda assim, seguem abaixo algumas imagens comprovativas da implementação de todos os passos acima mencionados. 

Repositório remoto - Versão final (é possível visualizar todos os *commits*, *branches*, *tags*):
![Fossil - Repositório Remoto](
RepositórioRemoto_Fossil.jpg)

*Branches* criados:
![Fossil - Branches](
Branches_Fossil.jpg)

*Tags* criadas:
![Fossil - Tags](
Tags_Fossil.jpg)

Local para a criação de *tickets* (*issues*):
![Fossil - Tickets](
Tickets_Fossil.jpg)


